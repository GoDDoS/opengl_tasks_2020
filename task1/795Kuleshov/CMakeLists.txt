add_compile_definitions(GLM_ENABLE_EXPERIMENTAL)

#include_directories(${CMAKE_SOURCE_DIR}/external/GLM)

set(SRC_FILES
        common/Application.cpp
        common/DebugOutput.cpp
        common/Camera.cpp
        common/Mesh.cpp
        common/ShaderProgram.cpp
        main.cpp
        common/Application.hpp
        common/DebugOutput.h
        common/Camera.hpp
        common/Mesh.hpp
        common/ShaderProgram.hpp
        )



MAKE_OPENGL_TASK(795Kuleshov 1 "${SRC_FILES}")