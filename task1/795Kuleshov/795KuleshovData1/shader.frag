#version 330

in float height;

out vec4 fragColor;

void main()
{
    fragColor = mix(vec4(0.7, 0.5, 0.3, 1), vec4(0.6, 0.3, 0, 1), height);
}