//
// Created by Igor on 05.03.2021.
//

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"

#include <iostream>
#include <vector>

class TerrainApplication : public Application
{
private:
    MeshPtr makeTerrain()
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<std::vector<float>> heights(30, std::vector<float>(30));
        std::vector<std::vector<glm::vec3>> prom_normals(30, std::vector<glm::vec3>(30));

        for (auto i = 0; i < 30; ++i) {
            for (auto j = 0; j < 30; ++j) {
                heights[i][j] = static_cast<float>(rand()) / RAND_MAX;
            }
        }

        for (auto i = 0; i < 29; ++i) {
            for (auto j = 0; j < 29; ++j) {
                vertices.push_back(glm::vec3(i, j, heights[i][j]));
                vertices.push_back(glm::vec3(i+1, j, heights[i+1][j]));
                vertices.push_back(glm::vec3(i+1, j+1, heights[i+1][j+1]));
                glm::vec3 triangle_normal = glm::cross(glm::vec3(1, 0, heights[i+1][j]-heights[i][j]), glm::vec3(1, 1, heights[i+1][j+1]-heights[i][j]));
                triangle_normal = glm::normalize(triangle_normal);
                prom_normals[i][j] += triangle_normal;
                prom_normals[i+1][j] += triangle_normal;
                prom_normals[i+1][j+1] += triangle_normal;
                vertices.push_back(glm::vec3(i, j, heights[i][j]));
                vertices.push_back(glm::vec3(i+1, j+1, heights[i+1][j+1]));
                vertices.push_back(glm::vec3(i, j+1, heights[i][j+1]));
                triangle_normal = glm::cross(glm::vec3(1, 1, heights[i+1][j+1]-heights[i][j]), glm::vec3(0, 1, heights[i][j+1]-heights[i][j]));
                triangle_normal = glm::normalize(triangle_normal);
                prom_normals[i][j] += triangle_normal;
                prom_normals[i][j+1] += triangle_normal;
                prom_normals[i+1][j+1] += triangle_normal;
            }
        }

        for (auto i = 0; i < 29; ++i) {
            for (auto j = 0; j < 29; ++j) {
                normals.push_back(glm::normalize(prom_normals[i][j]));
                normals.push_back(glm::normalize(prom_normals[i+1][j]));
                normals.push_back(glm::normalize(prom_normals[i+1][j+1]));
                normals.push_back(glm::normalize(prom_normals[i][j]));
                normals.push_back(glm::normalize(prom_normals[i+1][j+1]));
                normals.push_back(glm::normalize(prom_normals[i][j+1]));
            }
        }

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());

        return mesh;
    }
public:
    MeshPtr _terrain;

    ShaderProgramPtr _shader;

    void makeScene() override
    {
        Application::makeScene();

        _terrain = makeTerrain();
        _terrain->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        //=========================================================
        //������������� ��������

        _shader = std::make_shared<ShaderProgram>("795KuleshovData1/shader.vert", "795KuleshovData1/shader.frag");

    }

    void draw() override
    {
        Application::draw();

        //�������� ������� ������� ������ � ��������� �������
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //������� ������ ����� � ������� �� ����������� ���������� ����������� �����
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //���������� ������
        _shader->use();

        //��������� �� ���������� ������� ������ ����� � ��������� ���������
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        _shader->setMat4Uniform("modelMatrix", _terrain->modelMatrix());
        _terrain->draw();
    }
};

int main()
{
    TerrainApplication app;
    app.start();

    return 0;
}
